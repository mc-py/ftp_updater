import os
import requests
import shutil
import zipfile
import ftplib
from tkinter import *
from tkinter import messagebox
from functools import partial
import tkinter as tkk
import configparser
import urllib



# Main window
window = Tk()
window.title("FTP Updater")
window.geometry('940x680')

# Aplication name
appName = Label(window, text=('FTP Updater'), font='Helvetica 30')
appName.place(x=340, y=30)

# White output box in main window
outputBox = tkk.Text(window, width=85, height=18)
outputBox.place(x=130, y=230)
scr = Scrollbar(window, orient=VERTICAL, command=outputBox.yview)


def goToEnd():
    outputBox.update()
    outputBox.see("end")


# Window with information about FTP connection
def connectionInfo():
    connectionInfoWindow = Tk()
    connectionInfoWindow.title("Info o polaczeniu")
    connectionInfoWindowWidth = str(50+(7*(int(len("File with source and destination paths: "+srcDownloadFile)))))
    print(connectionInfoWindowWidth[:3])
    connectionInfoWindow.geometry(connectionInfoWindowWidth[:3]+'x200')
    ftpServerView = Label(connectionInfoWindow, text=('Serwer FTP: '+ftpServer), font='Helvetica 11')
    ftpServerView.grid(column=0, row=0, in_=connectionInfoWindow, sticky=W)

    ftpUserView = Label(connectionInfoWindow, text=('Server User: '+ftpUser), font='Helvetica 11')
    ftpUserView.grid(column=0, row=1, in_=connectionInfoWindow, sticky=W)

    pathToCCHFilesView = Label(connectionInfoWindow, text=("Temp folder path: "+pathToCCHFiles), font='Helvetica 11')
    pathToCCHFilesView.grid(column=0, row=2, in_=connectionInfoWindow, sticky=W)

    srcDownloadFileView = Label(connectionInfoWindow, text=("File with source and destination paths: "+srcDownloadFile), font='Helvetica 11')
    srcDownloadFileView.grid(column=0, row=3, in_=connectionInfoWindow, sticky=W)

    # FTP Connection check
    isconnectedLabel = Label(connectionInfoWindow, text=("FTP connection problems!"), font='Helvetica 11', bg="red")
    isconnectedLabel.grid(column=0, row=4, in_=connectionInfoWindow, sticky=W)
    isFtpAvaliable = ftplib.FTP(ftpServer, ftpUser, ftpPass)
    if isFtpAvaliable:
        isconnectedLabel = Label(connectionInfoWindow, text=("   FTP connection works!   "), font='Helvetica 11', bg="green")
        isconnectedLabel.grid(column=0, row=4, in_=connectionInfoWindow, sticky=W)
    else:
        isconnectedLabel = Label(connectionInfoWindow, text=("FTP connection problems!"), font='Helvetica 11', bg="red")
        isconnectedLabel.grid(column=0, row=4, in_=connectionInfoWindow, sticky=W)

# Scroll on white output box
scrollBox = scr.grid(row=2, column=2, rowspan=15, columnspan=1, sticky=NS)
outputBox.tag_config('warning', background="yellow", foreground="red")
outputBox.tag_config('info', background="white", foreground="blue")
outputBox.tag_config('done', background="black", foreground="green")
outputBox.tag_config('upload', background="white", foreground="green")
outputBox.config(yscrollcommand=scr.set, font=('Arial', 10, 'italic'), )
scr.place(x=890, y=230, height=365)


#tex.config(yscrollcommand=scr.set, font=('Arial', 8, 'bold', 'italic'))

pathToLocalFolder = (os.path.dirname(os.path.abspath(__file__)))
pathToCCHFiles = (pathToLocalFolder+'\\TempCH')
srcDownloadFile = (pathToLocalFolder+'\\download_binaries.txt')


pathToCfg = (pathToLocalFolder+'\\config.txt')
cfgParser = configparser.RawConfigParser()
cfgParser.readfp(open(pathToCfg))
ftpServer = cfgParser.get('CFG', 'ftp_server')
ftpUser = cfgParser.get('CFG', 'ftp_user')
ftpPass = cfgParser.get('CFG', 'ftp_pw')


serviceNameTxt = Label(window, text=("Service name: "), font='Helvetica 11')
serviceNameTxt.place(x=130, y=140, in_=window)
serviceNameEntry = Entry(window, width=30)
serviceNameEntry.place(x=280, y=140, in_=window)
#service = (serviceNameEntry.get())


serviceVersionTxt = Label(window, text=("Service version: "), font='Helvetica 11')
serviceVersionTxt.place(x=130, y=170, in_=window)
serviceVersionEntry = Entry(window, width=30)
serviceVersionEntry.place(x=280, y=170, in_=window)


def startup():
    global version
    global service
    global sourcePath
    global destPath
    global serviceName  # wyszukana nazwa serwisu
    global specyfiedLinestr
    service = serviceNameEntry.get()
    version = serviceVersionEntry.get()
    showFile=open(srcDownloadFile, 'r').readlines() # open source file
    specyfiedLine = [s for s in showFile if service in s] # search line in source file with service name
    specyfiedLinestr = str(specyfiedLine).replace("['","").replace("']","").replace("\\\\", "\\").replace("\n", "")[:-2] #remove unnecessary marks and change this list into string
    spacesinResults = specyfiedLinestr.count(" ") #count spaces in search results
    if spacesinResults > 1: #too many spaces - too many results
        messagebox.showinfo("Info",("Za duzo wynikow. Prosze o sprecyzowanie wyszukiwania"))

        #po znalezeniu wiecej wynikow niz 2 wybieramy gdzie chcemy uploadowac

        goToEnd();
        window.mainloop()
    elif spacesinResults < 1: # service not found
        messagebox.showinfo("Info",("Nie ma zadnego wpisu dotyczacego: "+service))
        goToEnd();
        window.mainloop()
    else:
        sourcePath,destPath=specyfiedLinestr.split(" ")
        if sourcePath.endswith("/"):
            sourcePath = sourcePath[:-1]
        serviceName = (sourcePath.rsplit('/',1)[1])
        correctService = messagebox.askyesno("Pytanko",("Czy chodzilo Ci o serwis: "+serviceName))
        if correctService:
            versionPart();
        else:
            window.mainloop()


def versionPart():
    global versionHttpFolder
    versionHttpFolder = (sourcePath+"/"+version)
    r = requests.head(versionHttpFolder)
    if r:
        outputBox.insert(END,"Wyszukuje wersji: "+version+" serwisu: "+ serviceName+" na mavenie\n\n", 'info')
        goToEnd();
        #checkBinZipOrAarExists();
        findBin();
    else:
        outputBox.insert(END,"Nie ma takiej wersji na maven.dev\n\n", 'warning')
        goToEnd();
        window.mainloop()


def findBin():
    global zipScriptsFile
    global zipText
    global scriptsText
    global binFile
    global scriptsZipFile
    global lookingForZipFilesList
    zipText = ('.zip"')
    aarText = ('.aar"')
    scriptsText = ("scripts")
    outputBox.insert(END, "Tworzenie tymczasowego folderu na pliki: "+pathToCCHFiles+"\n\n", 'info')
    goToEnd();
    os.mkdir(pathToCCHFiles)
    webPage = urllib.request.urlopen(versionHttpFolder).read().decode("utf-8") #open page and decode
    webPageLocalfilePath = (pathToCCHFiles+'\\webcontext.txt')  #dest file where save whole webpage
    file = open(webPageLocalfilePath, 'w')         #open file where to write
    file.write(webPage)                            #write
    file.close()                                   #close save session
    showFile = open(webPageLocalfilePath, 'r').readlines()
    lookingForAarFiles = [s for s in (open(webPageLocalfilePath, 'r').readlines()) if aarText in s]
    lookingForZipFiles = [s for s in (open(webPageLocalfilePath, 'r').readlines()) if zipText in s]
    howManyAarFilesCount = 0    #zwraca ile jest plikow AAR na stronie
    howManyZipFilesCount = 0    #zwraca ile jest plikow ZIP na stronie
    print ("findBin: ",destPath)
    for howManyAarFiles in lookingForAarFiles:
        howManyAarFilesCount = howManyAarFilesCount +1
    for howManyZipFiles in lookingForZipFiles:
        howManyZipFilesCount = howManyZipFilesCount +1
    lookingForZipFiles = str(lookingForZipFiles)
    lookingForZipFilesList = []
    lookingForZipFilesList = lookingForZipFiles.split(',')

    if howManyAarFilesCount == 1:
        lookingForBinFile = str(lookingForAarFiles)
        lookingForAarFilesList = []
        lookingForAarFilesList = lookingForBinFile.split(',')
        aarMavenFiles = [] #lista ze wszystkimi plikami AAR
        #for webZipFile in lookingForAarFilesList:
        beforeZip,afterZip = lookingForBinFile.split("href=\"")
        randomAarFile,restt = afterZip.split('.aar\"')
        randomAarFile = (randomAarFile+".aar")
        aarMavenFiles.append(randomAarFile)
        #print ("plik AAR to: ",randomAarFile)
        binFile = randomAarFile
        outputBox.insert(END,"Ilosc plikow aar: "+str(howManyAarFilesCount)+"\nNazwa pliku bin: "+str(binFile)+". Zaczynam szukac skryptow...\n\n", 'info')
        goToEnd();
        findScripts();

    elif howManyZipFilesCount == 0:
        outputBox.insert(END,"Nie znaleziono zadnych plikow zip. Sprawdz poprawnosc sciezki do mavena\n\n", 'warning')
        goToEnd();
        window.mainloop();

    elif howManyZipFilesCount == 2:
        lookingForZipFilesStr = str(lookingForZipFiles)
        firstZip,secondZip = lookingForZipFilesStr.split(",")
        beforeZip,afterZip = firstZip.split("href=\"")
        serviceName,restt = afterZip.split('.zip\"')
        serviceName = (serviceName+".zip")
        beforeZip2,afterZip2 = secondZip.split("href=\"")
        serviceName2,restt2 = afterZip2.split('.zip\"')
        serviceName2 = (serviceName2+".zip")
        wordScripts = ("scripts")
        print ("howManyZipFilesCount == 2: "+destPath)
        if wordScripts in serviceName:
            scriptsZipFile = serviceName
            binFile = serviceName2
            outputBox.insert(END,"Ilosc plikow zip: "+str(howManyZipFilesCount)+"\nBinarka: "+str(binFile)+"\nSkrypty: "+str(scriptsZipFile)+"\nZaczynam sciagac pliki do lokalnego folderu...\n\n", 'info')
            goToEnd();
            checkScriptsZipExists();
        elif wordScripts in serviceName2:
            scriptsZipFile = serviceName2
            binFile = serviceName
            outputBox.insert(END,"Ilosc plikow zip: "+str(howManyZipFilesCount)+"\nBinarka: "+str(binFile)+"\nSkrypty: "+str(scriptsZipFile)+"\nZaczynam sciagac pliki do lokalnego folderu...\n\n", 'info')
            goToEnd();
            checkScriptsZipExists();
        else:
            outputBox.insert(END,"Cos tu jest nie tak...\n\n", 'warning')
            goToEnd();


    elif howManyZipFilesCount > 2:
        global howManynotScriptsZipFilesCount
        global notScriptsZipFiles
        zipMavenFiles = []
        notScriptsZipFiles = []
        for webZipFile in lookingForZipFilesList:
            beforeZip,afterZip = webZipFile.split("href=\"")
            randomZipFile,restt = afterZip.split('.zip\"')
            randomZipFile = (randomZipFile+".zip")
            zipMavenFiles.append(randomZipFile)
        howManynotScriptsZipFilesCount = 0                              #ilosc plikow *.zip NIE majacaych w nazwie "scripts"
        notScriptsZipFiles = [x for x in zipMavenFiles if "scripts" not in x]
        for howManynotScriptsZipFiles in notScriptsZipFiles:
            howManynotScriptsZipFilesCount = howManynotScriptsZipFilesCount + 1
        howManyzipScriptsFilesCount = 0                                 #ilosc plikow *.zip majacaych w nazwie "scripts"
        zipScriptsFile = [x for x in zipMavenFiles if "scripts" in x]
        for howManyzipScriptsFile in zipScriptsFile:
            howManyzipScriptsFilesCount = howManyzipScriptsFilesCount + 1
        scriptsZipFile = zipScriptsFile
        #print ("pliki ktore NIE sa skryptami to: ",notScriptsZipFiles," mamy ich ",howManynotScriptsZipFilesCount)
        #print ("pliki ktore SA skryptami to: ",zipScriptsFile," mamy ich ",howManyzipScriptsFilesCount)
        askAboutFileWindow();

    else:
        outputBox.insert(END,"Cos tu jest nie tak...\n\n", 'warning')
        goToEnd();
        window.mainloop();



def findScripts():
    global notScriptsZipFiles
    global scriptsZipFile
    webPageLocalfilePath = (pathToCCHFiles+'\\webcontext.txt')
    lookingForScriptsZipFiles = [s for s in (open(webPageLocalfilePath, 'r').readlines()) if zipText in s]
    lookingForScriptsZipFiles = str(lookingForScriptsZipFiles)
    lookingForScriptsZipFilesList = []
    lookingForScriptsZipFilesList = lookingForScriptsZipFiles.split(',')
    numersOfScriptsZipFiles = 0
    zipMavenScriptsFiles = []
    notScriptsZipFiles = []
    for webScriptsZipFile in lookingForZipFilesList:
        beforeScriptsZip,afterScriptsZip = webScriptsZipFile.split("href=\"")
        randomScriptsZipFile,restt = afterScriptsZip.split('.zip\"')
        randomScriptsZipFile = (randomScriptsZipFile+".zip")
        ###print(serviceNamePosition)
        zipMavenScriptsFiles.append(randomScriptsZipFile)

    zipScriptsFile = [x for x in zipMavenScriptsFiles if "scripts" in x]
    scriptsZipFile = zipScriptsFile                                     #pelna nazwa pliku scripts
    checkScriptsZipExists();


def askAboutFileWindow():
    global buttons
    global fileWindow
    global zipFileNo
    fileWindow = Tk()
    fileWindow.title("Ktory plik?")
    windowHeight = (100+60*(int(howManynotScriptsZipFilesCount)))
    fileWindowResolution = ('400x'+str(windowHeight))
    fileWindow.geometry(fileWindowResolution)

    fileWindowInfo = Label(fileWindow, text='Wybierz plik BIN ktory chcesz zuploadowac\nna FTP', font='Helvetica 12')
    fileWindowInfo.place(x=50, y=10, anchor=CENTER)
    fileWindowInfo.pack(ipady = 20)

    buttonY = 50
    for buttons in notScriptsZipFiles:
        action_with_arg = partial(setZipFile, buttons)
        firstZipFileButton = Button(fileWindow, text=buttons, command=action_with_arg)
        firstZipFileButton.place(x=20, y=buttonY, width=400, height=30, anchor=CENTER)
        firstZipFileButton.pack()
        buttonY = buttonY + 40


def setZipFile(selectedZipFile):
    global binFile
    binFile = selectedZipFile
    outputBox.insert(END,"Wybrales plik bin do uploadu: "+selectedZipFile+"\n\n", 'info')
    goToEnd();
    fileWindow.destroy()
    checkScriptsZipExists();


def checkScriptsZipExists():
    global pathToSciptsFile
    global pathToBinZipFile
    pathToBinZipFile = (versionHttpFolder+"/"+binFile)
    pathToSciptsFile=(versionHttpFolder+"/"+serviceName+"-"+version+"-installer-scripts.zip")
    o = requests.head(pathToSciptsFile)
    if o:
        outputBox.insert(END, "Sciezka do skryptow: "+pathToSciptsFile+"\n\n", 'info')
        goToEnd();
        downloadFilesToLocalPC();
    else:
        outputBox.insert(END, "Sprawdz nazwe pliku scripts w: "+(specyfiedLinestr.split(" ")[0])+"\n\n", 'warning')
        goToEnd();
        window.mainloop()


def downloadFilesToLocalPC():
    global binLocal
    global scriptsLocal
    global pathToLocalAppVersion
    global scriptsZipFileStr
    global binFileSize
    pathToLocalAppVersion=(pathToCCHFiles+"\\"+serviceName+"\\"+version)
    os.makedirs(pathToCCHFiles+"\\"+serviceName+"\\"+version+"\\bin",exist_ok=True)
    os.makedirs(pathToCCHFiles+"\\"+serviceName+"\\"+version+"\\scripts",exist_ok=True)
    binLocal=(pathToCCHFiles+"\\"+serviceName+"\\"+version+"\\bin")
    scriptsLocal=(pathToCCHFiles+"\\"+serviceName+"\\"+version+"\\scripts")
    outputBox.insert(END,"Kopiowanie plików z mavena do tymczasowego folderu\n\n", 'info')
    goToEnd();
    scriptsZipFileStr = str(scriptsZipFile).replace("['","").replace("']","")
    t = requests.get(pathToBinZipFile)
    with open(binLocal+"\\"+binFile, 'wb') as openBin:
        openBin.write(t.content)
    r = requests.get(pathToSciptsFile)
    with open(scriptsLocal+"\\"+scriptsZipFileStr, 'wb') as openScripts:
        openScripts.write(r.content)
    binFileSize = (os.stat(binLocal+"\\"+binFile)).st_size
    outputBox.insert(END,"Zapisano plik w "+binLocal+"\n\n", 'info')
    goToEnd();
    print ("downloadFilesToLocalPC: "+destPath)
    unzipScripts();


def unzipScripts():
    outputBox.insert(END,"Wypakowuje skrypty...\n\n", 'info')
    goToEnd();
    ZipFile=''
    zip_ref = zipfile.ZipFile((scriptsLocal+"\\"+scriptsZipFileStr), 'r')
    zip_ref.extractall(pathToCCHFiles+"\\"+serviceName+"\\"+version)
    zip_ref.close()
    outputBox.insert(END,"Skrypty wypakowane!\n\n", 'info')
    goToEnd();
    os.remove(scriptsLocal+"\\"+scriptsZipFileStr)
    #checkFTP();
    updateBinOnFTP()

def checkFTP():
    ftpp = ftplib.FTP(ftpServer)
    ftpp.login(ftpUser,ftpPass)
    print ("checkFTP: "+destPath)
    ftpCheckVersion = str(ftpp.nlst(destPath+version))
    oldVersions = int(ftpCheckVersion.count(version+'_OLD'))+1
    folderName = str(destPath+version)
    if folderName in ftpCheckVersion:
        overwriteFtp = messagebox.askyesno("Pytanko",("Taka wersja jest już na FTP!\n\nCzy mam obecna wersje wy-OLD-owac?\n\n"+"Nowa nazwa obecnej wersji na FTP to: "+folderName+'_OLD'+str(oldVersions)+"\n\n"+"Wersja ktora pobrales bedzie zapisana pod: "+folderName+"\n\n"))
        overwriteFtp
        if overwriteFtp:
            outputBox.insert(END,'Obecna wersja zmienia nazwe na: '+folderName+'_OLD'+str(oldVersions)+"\n\n", 'info')
            goToEnd();
            ftpp.rename(folderName,folderName+'_OLD'+str(oldVersions))
            ftpp.close()
            updateBinOnFTP();
        else:
            outputBox.insert(END,"To wracamy na poczatek", 'warning')
            goToEnd();
            window.mainloop()
    else:
        updateBinOnFTP();

sizeWritten = 0

def progressBar(block):
    global sizeWritten
    sizeWritten += 32768
    percentComplete = (str((sizeWritten / binFileSize)*100)[:4])
    percent,rest = beforeZip,afterZip = percentComplete.split(".")
    outputBox.insert(END, percent+"% of complete\n")
    outputBox.see("end")
    outputBox.update()


def progressBarWindow():
    mGui = Tk()

    mGui.geometry('450x450')
    mGui.title('Bin file upload')

    mpb = Progressbar(mGui,orient ="horizontal",length = 200, mode ="determinate")
    mpb.pack()
    mpb["maximum"] = binFileSize
    mpb["value"] = 32768
    mGui.mainloop()


def updateBinOnFTP():
    outputBox.insert(END,'Uploaduje plik: '+binFile+"\n\n", 'info')
    goToEnd();
    session = ftplib.FTP(ftpServer,ftpUser,ftpPass)
    ftpBinFile = open(binLocal+'\\'+binFile,'rb')    #binFile to send
    print (destPath+"/"+version+'/bin')
    session.mkd(destPath+"/"+version)
    session.mkd(destPath+"/"+version+'/bin')         #create direcotry with version\bin
    session.mkd(destPath+"/"+version+'/scripts')     #create direcotry with version\scripts
    progressBarWindow();
    session.storbinary('STOR '+destPath+'\\'+version+'\\bin\\'+binFile,ftpBinFile,callback=progressBar,blocksize=32768)
    progressBarWindow.destroy()
    ftpBinFile.close()                              # close file and FTP
    session.quit()
    listOfScriptsFiles();



def listOfScriptsFiles():
    files = []
    for r, d, f in os.walk(scriptsLocal):
        for file in f:
            if '.' in file:
                files.append(os.path.join(r, file))

    for f in files:
        scriptFileToUpload = f.rsplit('\\',1)[1]
        outputBox.insert(END,'Uploaduje plik: '+scriptFileToUpload+"\n", 'info')
        goToEnd();
        session = ftplib.FTP(ftpServer,ftpUser,ftpPass)
        ftpScriptFile = open(f,'rb')                            #scriptFile to send
        session.storbinary('STOR '+destPath+'\\'+version+'\\scripts\\'+scriptFileToUpload,ftpScriptFile)
        ftpScriptFile.close()                              # close file and FTP
        session.quit()
    finish();


def finish():
    outputBox.insert(END,'Gotowe. Usuwam TempCH\n', 'done')
    goToEnd();
    shutil.rmtree(pathToCCHFiles, ignore_errors=True)


def clearTextBox():
    outputBox.delete('1.0', END)
    window.mainloop()



#Buttons
searchButton = Button(window, text="Search",command=startup)
searchButton.place(x=500, y=135, in_=window,width=70,height=60)
clearButton = Button(window, text="Clear\nlog\nwindow",command=clearTextBox)
clearButton.place(x=580, y=135, in_=window,width=70,height=60)
connectionInfoButton = Button(window, text="Connection\ninfo",command=connectionInfo)
connectionInfoButton.place(x=660, y=135, in_=window,width=70,height=60)



window.mainloop()