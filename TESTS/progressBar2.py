import os
from tqdm import tqdm
import tkinter
from tkinter import ttk
import requests
import tkinter as tkk
import os
import requests
import shutil
import zipfile
import ftplib
from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from functools import partial
import tkinter as tkk
import configparser
import urllib



window = Tk()
window.title("FTP Updater")
window.geometry('940x680')


#root = Tk()
#root.title("Uploading...")
#root.geometry('300x100')


ftpServer = ''
ftpUser = 'ftp_deploy_admin'
ftpPass = ''



# White output box in main window
outputBox = tkk.Text(window, width=5, height=1)
outputBox.place(x=505, y=230)

loadingBar = tkk.Frame(window, width=100, height=2)
loadingBar.place(x=370, y=260)


filePath = 'c:\\py\\FTP_Updater_v6.2.1\\testPath\\spc-jobs-0.40.0-installer-bin.zip'
fileName = 'spc-jobs-0.40.0-installer-bin.zip'
binFolderPath = 'c:\py\FTP_Updater_v6.2.1\testPath'
httpFilePath = 'http://maven.dev.sys/archiva/repository/release/com/crif/spc/spc-jobs/0.40.0/spc-jobs-0.40.0-installer-bin.zip'
ftpPath = '/deliverables/deltavista/test/spc-jobs/'

totalSize = (os.stat(filePath)).st_size
sizeWritten = 0



def progressBar(block):
    global sizeWritten
    sizeWritten += 65536
    percentComplete = (str((sizeWritten / totalSize)*100)[:4])
    percent,rest = beforeZip,afterZip = percentComplete.split(".")
    outputBox.insert(END, "\n"+percent+"%")
    outputBox.see("end")
    outputBox.update()
    print (sizeWritten)
    pb["value"]=sizeWritten
    pb["maximum"]=totalSize
    pb.update()


def upload():
    global pb
    session = ftplib.FTP(ftpServer,ftpUser,ftpPass)
    ftpBinFile = open(filePath,'rb')    #binFile to send
    pb = ttk.Progressbar(loadingBar, orient='horizontal', length=300, mode='determinate')
    pb.pack(expand=True, fill=tkinter.BOTH, side=tkinter.TOP)
    session.storbinary('STOR '+ftpPath+'\\'+fileName,ftpBinFile,callback=progressBar,blocksize=65536)
    pb.destroy()
    ftpBinFile.close()                              # close file and FTP
    session.quit()



searchButton = Button(window, text="upload",command=upload)
searchButton.place(x=500, y=135, in_=window,width=70,height=60)


window.mainloop()