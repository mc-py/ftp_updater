import os
import requests
import shutil
import zipfile
import ftplib
from tkinter import *
from tkinter import messagebox
from functools import partial
import tkinter as tkk
import configparser
import urllib
from tkinter import ttk
import tkinter
import sys



######################################################################
# Main window
window = Tk()
window.title("FTP Updater")
window.geometry('1000x680')

# Aplication name
appName = Label(window, text=('FTP Updater'), font='Helvetica 30', fg="#006600")
appName.place(x=350, y=30)


# White output box in main window
outputBox = tkk.Text(window, width=90, height=19)
outputBox.place(x=130, y=230)
scr = Scrollbar(window, orient=VERTICAL, command=outputBox.yview)


def goToEnd():
    outputBox.update()
    outputBox.see("end")


# Scroll on white output box
scrollBox = scr.grid(row=2, column=2, rowspan=15, columnspan=1, sticky=NS)
outputBox.tag_config('warning', background="#ffff99", foreground="#ff0000")
outputBox.tag_config('info', background="white", foreground="#000000")
outputBox.tag_config('done', background="#99e699", foreground="#28a428")
outputBox.tag_config('upload', background="white", foreground="green")
outputBox.config(yscrollcommand=scr.set, font=('Times', 12))
scr.place(x=853, y=230, height=365)


pathToLocalFolder = (os.path.dirname(os.path.abspath(__file__)))
pathToCCHFiles = (pathToLocalFolder+'\\TempCH')
srcDownloadFile = (pathToLocalFolder+'\\download_binaries.txt')


pathToCfg = (pathToLocalFolder+'\\config.txt')
cfgParser = configparser.RawConfigParser()
cfgParser.readfp(open(pathToCfg))
ftpServer = cfgParser.get('CFG', 'ftp_server')
ftpUser = cfgParser.get('CFG', 'ftp_user')
ftpPass = cfgParser.get('CFG', 'ftp_pw')



#####################################################################################


itemsforlistbox = []


serviceNameTxt = Label(window, text=("Service name: "), font='Helvetica 12')
serviceNameTxt.place(x=130, y=138, in_=window)
serviceNameEntry = Entry(window, width=40)
serviceNameEntry.place(x=260, y=140, in_=window)


serviceVersionTxt = Label(window, text=("Service version: "), font='Helvetica 12')
serviceVersionTxt.place(x=130, y=178, in_=window)
serviceVersionEntry = Entry(window, width=40)
serviceVersionEntry.place(x=260, y=180, in_=window)




def startup():
    global version
    global service
    global specyfiedLine
    global choosedLine
    service = serviceNameEntry.get()
    version = serviceVersionEntry.get()
    showFile=open(srcDownloadFile, 'r').readlines() # open source file
    specyfiedLine = [s for s in showFile if service in s] # search line in source file with service name
    if not specyfiedLine:
        messagebox.showinfo("Info",("Nie ma zadnego wpisu dotyczacego: "+service))
        goToEnd();
        window.mainloop()
    specyfiedLineCount = int(str(specyfiedLine).count(",")+1) #count comma in search results
    if specyfiedLineCount > 1: #too many spaces - too many results
        #messagebox.showinfo("Info",("Za duzo wynikow. Prosze o sprecyzowanie wyszukiwania wybierajac interesujaca Cie linie sciagajaca"))
        for eachLine in specyfiedLine:
            itemsforlistbox.append(eachLine)
        selectSpecyfiedline()
        goToEnd();
        window.mainloop()
    else:
        choosedLine = str(specyfiedLine[0])
        goToEnd();
        splitDownloadLine()


def selectSpecyfiedline():
    global itemsforlistbox
    global chooseWindow
    chooseWindow = Tk()
    chooseWindow.title("Wybierz linie sciagajaca")
    chooseWindow.geometry('1400x380')
    appName = Label(chooseWindow, text=('Za duzo wynikow. Prosze o sprecyzowanie wyszukiwania wybierajac interesujaca Cie linie sciagajaca:\n'), font='Helvetica 12', fg="#cc5200")
    appName.place(x=32, y=30)
    def CurSelet(evt):
        global choosedLine
        itemsforlistbox=str((listBox.get(listBox.curselection())))
        choosedLine = str(itemsforlistbox)
        closeChooseWindow()

    listBox=Listbox(chooseWindow,width=148,height=8,font=('airal',12,'underline'),fg='#0077b3')
    listBox.bind('<<ListboxSelect>>',CurSelet)
    listBox.place(x=32,y=70)

    for items in itemsforlistbox:
        listBox.insert(END,items)
    chooseWindow.mainloop()


def closeChooseWindow():
    if 'normal' == chooseWindow.state():
        chooseWindow.destroy()
        itemsforlistbox.clear()
        splitDownloadLine()
    goToEnd();
    window.mainloop()


def splitDownloadLine():
    global sourcePath
    global destPath
    global serviceName  # wyszukana nazwa serwisu
    sourcePath,destPath = choosedLine.split(" ")
    if sourcePath.endswith("/"):
        sourcePath = sourcePath[:-1]
    if destPath.endswith("\n"):
        destPath = destPath[:-1]
    if destPath.endswith("/"):
        destPath = destPath[:-1]
    serviceName = (sourcePath.rsplit('/',1)[1])
    outputBox.insert(END,"Linia sciagajaca to:\n"+choosedLine+"\n", 'info')
    goToEnd();
    correctService = messagebox.askyesno("Pytanko",("Czy chodzilo Ci o serwis: "+serviceName))
    if correctService:
        versionPart();
    else:
        delteAllAfterStart()
        window.mainloop()



def versionPart():
    global versionHttpFolder
    versionHttpFolder = (sourcePath+"/"+version)
    r = requests.head(versionHttpFolder)
    if r:
        outputBox.insert(END,"Wyszukuje wersji: "+version+" serwisu: "+ serviceName+" na mavenie\n\n", 'info')
        goToEnd();
        #checkBinZipOrAarExists();
        findBin();
    else:
        outputBox.insert(END,"Nie ma takiej wersji na maven.dev\n\n", 'warning')
        goToEnd();
        window.mainloop()


def findBin():
    global zipScriptsFile
    global zipText
    global scriptsText
    global binFile
    global scriptsZipFile
    global lookingForZipFilesList
    global zipMavenFiles
    global notScriptsZipFiles
    zipText = ('.zip"')
    aarText = ('.aar"')
    scriptsText = ("scripts")
    outputBox.insert(END, "Tworzenie tymczasowego folderu na pliki: "+pathToCCHFiles+"\n\n", 'info')
    goToEnd();
    os.mkdir(pathToCCHFiles)
    webPage = urllib.request.urlopen(versionHttpFolder).read().decode("utf-8") #open page and decode
    webPageLocalfilePath = (pathToCCHFiles+'\\webcontext.txt')  #dest file where save whole webpage
    file = open(webPageLocalfilePath, 'w')         #open file where to write
    file.write(webPage)                            #write
    file.close()                                   #close save session
    showFile = open(webPageLocalfilePath, 'r').readlines()
    lookingForAarFiles = [s for s in (open(webPageLocalfilePath, 'r').readlines()) if aarText in s]
    lookingForZipFiles = [s for s in (open(webPageLocalfilePath, 'r').readlines()) if zipText in s]
    howManyAarFilesCount = 0    #zwraca ile jest plikow AAR na stronie
    howManyZipFilesCount = 0    #zwraca ile jest plikow ZIP na stronie
    for howManyAarFiles in lookingForAarFiles:
        howManyAarFilesCount = howManyAarFilesCount +1
    for howManyZipFiles in lookingForZipFiles:
        howManyZipFilesCount = howManyZipFilesCount +1
    lookingForZipFiles = str(lookingForZipFiles)


    lookingForZipFilesList = []
    lookingForZipFilesList = lookingForZipFiles.split(',')
    zipMavenFiles = []
    notScriptsZipFiles = []
    for webZipFile in lookingForZipFilesList:
        beforeZip,afterZip = webZipFile.split("href=\"")
        randomZipFile,restt = afterZip.split('.zip\"')
        randomZipFile = (randomZipFile+".zip")
        zipMavenFiles.append(randomZipFile)
    lookingForZipFilesList.clear()


    if howManyAarFilesCount == 1:
        lookingForBinFile = str(lookingForAarFiles)
        lookingForAarFilesList = []
        lookingForAarFilesList = lookingForBinFile.split(',')
        aarMavenFiles = [] #lista ze wszystkimi plikami AAR
        #for webZipFile in lookingForAarFilesList:
        beforeZip,afterZip = lookingForBinFile.split("href=\"")
        randomAarFile,restt = afterZip.split('.aar\"')
        randomAarFile = (randomAarFile+".aar")
        aarMavenFiles.append(randomAarFile)
        #print ("plik AAR to: ",randomAarFile)
        binFile = randomAarFile
        outputBox.insert(END,"Ilosc plikow aar: "+str(howManyAarFilesCount)+"\nNazwa pliku bin: "+str(binFile)+". Zaczynam szukac skryptow...\n\n", 'info')
        goToEnd();
        checkHowManyScriptsFiles();

    elif howManyZipFilesCount == 0:
        outputBox.insert(END,"Nie znaleziono zadnych plikow zip. Sprawdz poprawnosc sciezki do mavena\n\n", 'warning')
        goToEnd();
        window.mainloop();

    elif howManyZipFilesCount == 2:
        lookingForZipFilesStr = str(lookingForZipFiles)
        firstZip,secondZip = lookingForZipFilesStr.split(",")
        beforeZip,afterZip = firstZip.split("href=\"")
        serviceName,restt = afterZip.split('.zip\"')
        serviceName = (serviceName+".zip")
        beforeZip2,afterZip2 = secondZip.split("href=\"")
        serviceName2,restt2 = afterZip2.split('.zip\"')
        serviceName2 = (serviceName2+".zip")
        wordScripts = ("scripts")
        if wordScripts in serviceName:
            scriptsZipFile = serviceName
            binFile = serviceName2
            outputBox.insert(END,"Ilosc plikow zip: "+str(howManyZipFilesCount)+"\nBinarka: "+str(binFile)+"\nSkrypty: "+str(scriptsZipFile)+"\n\nZaczynam sciagac pliki do lokalnego folderu...\n", 'info')
            goToEnd();
            checkScriptsZipExists();
        elif wordScripts in serviceName2:
            scriptsZipFile = serviceName2
            binFile = serviceName
            outputBox.insert(END,"Ilosc plikow zip: "+str(howManyZipFilesCount)+"\nBinarka: "+str(binFile)+"\nSkrypty: "+str(scriptsZipFile)+"\nZaczynam sciagac pliki do lokalnego folderu...\n", 'info')
            goToEnd();
            checkScriptsZipExists();
        else:
            outputBox.insert(END,"Cos tu jest nie tak...\n\n", 'warning')
            goToEnd();


    elif howManyZipFilesCount > 2:
        global howManynotScriptsZipFilesCount
        notScriptsZipFiles = []
        howManynotScriptsZipFilesCount = 0                              #ilosc plikow *.zip NIE majacaych w nazwie "scripts"
        notScriptsZipFiles = [x for x in zipMavenFiles if "scripts" not in x]
        for howManynotScriptsZipFiles in notScriptsZipFiles:
            howManynotScriptsZipFilesCount = howManynotScriptsZipFilesCount + 1
        if howManynotScriptsZipFilesCount > 1:
            askAboutFileWindow();
        else:
            binFile = notScriptsZipFiles[0]
            checkHowManyScriptsFiles()

    else:
        outputBox.insert(END,"Cos tu jest nie tak...\n\n", 'warning')
        goToEnd();
        window.mainloop();



def checkHowManyScriptsFiles():
    global scriptsZipFile
    global howManyzipScriptsFilesCount
    global zipScriptsFile
    howManyzipScriptsFilesCount = 0                                #ilosc plikow *.zip majacaych w nazwie "scripts"
    zipScriptsFile = [x for x in zipMavenFiles if "scripts" in x]
    for howManyzipScriptsFile in zipScriptsFile:
        howManyzipScriptsFilesCount = howManyzipScriptsFilesCount + 1
    if howManyzipScriptsFilesCount > 1:
        askAboutScriptsFileWindow()
    else:
        scriptsZipFile = zipScriptsFile[0]
        checkScriptsZipExists();


def findScripts():
    global notScriptsZipFiles
    global scriptsZipFile
    webPageLocalfilePath = (pathToCCHFiles+'\\webcontext.txt')
    lookingForScriptsZipFiles = [s for s in (open(webPageLocalfilePath, 'r').readlines()) if zipText in s]
    lookingForScriptsZipFiles = str(lookingForScriptsZipFiles)
    lookingForScriptsZipFilesList = []
    lookingForScriptsZipFilesList = lookingForScriptsZipFiles.split(',')
    numersOfScriptsZipFiles = 0
    zipMavenScriptsFiles = []
    notScriptsZipFiles = []
    for webScriptsZipFile in lookingForZipFilesList:
        beforeScriptsZip,afterScriptsZip = webScriptsZipFile.split("href=\"")
        randomScriptsZipFile,restt = afterScriptsZip.split('.zip\"')
        randomScriptsZipFile = (randomScriptsZipFile+".zip")
        zipMavenScriptsFiles.append(randomScriptsZipFile)
    zipScriptsFile = [x for x in zipMavenScriptsFiles if "scripts" in x]
    if howManyzipScriptsFilesCount > 1:
        askAboutScriptsFileWindow()
    else:
        scriptsZipFile = zipScriptsFile[0]                                     #pelna nazwa pliku scripts
    checkScriptsZipExists();


def askAboutScriptsFileWindow():
    global buttons2
    global scriptsFileWindow
    global zipFileNo
    scriptsFileWindow = Tk()
    scriptsFileWindow.title("Ktory plik?")
    windowHeight = (100+60*(int(howManyzipScriptsFilesCount)))
    scriptsFileWindowResolution = ('400x'+str(windowHeight))
    scriptsFileWindow.geometry(scriptsFileWindowResolution)
    scriptsFileWindowInfo = Label(scriptsFileWindow, text='Wybierz plik ktory chcesz zuploadowac\nna FTP', font='Helvetica 12')
    scriptsFileWindowInfo.place(x=50, y=10, anchor=CENTER)
    scriptsFileWindowInfo.pack(ipady = 20)
    buttonY = 50
    for buttons2 in zipScriptsFile:
        action_with_arg = partial(setScriptsZipFile, buttons2)
        firstZipFileButton = Button(scriptsFileWindow, text=buttons2, command=action_with_arg)
        firstZipFileButton.place(x=20, y=buttonY, width=400, height=30, anchor=CENTER)
        firstZipFileButton.pack()
        buttonY = buttonY + 40




def setScriptsZipFile(selectedScriptsZipFile):
    global scriptsZipFile
    scriptsZipFile = selectedScriptsZipFile
    outputBox.insert(END,"Wybrales plik scripts do uploadu: "+scriptsZipFile+"\n\n", 'info')
    goToEnd();
    scriptsFileWindow.destroy()
    checkScriptsZipExists();



def askAboutFileWindow():
    global buttons
    global fileWindow
    global zipFileNo
    fileWindow = Tk()
    fileWindow.title("Ktory plik?")
    windowHeight = (100+60*(int(howManynotScriptsZipFilesCount)))
    fileWindowResolution = ('400x'+str(windowHeight))
    fileWindow.geometry(fileWindowResolution)
    fileWindowInfo = Label(fileWindow, text='Wybierz plik ktory chcesz zuploadowac\nna FTP', font='Helvetica 12')
    fileWindowInfo.place(x=50, y=10, anchor=CENTER)
    fileWindowInfo.pack(ipady = 20)
    buttonY = 50
    for buttons in notScriptsZipFiles:
        action_with_arg = partial(setZipFile, buttons)
        firstZipFileButton = Button(fileWindow, text=buttons, command=action_with_arg)
        firstZipFileButton.place(x=20, y=buttonY, width=400, height=30, anchor=CENTER)
        firstZipFileButton.pack()
        buttonY = buttonY + 40



def setZipFile(selectedZipFile):
    global binFile
    binFile = selectedZipFile
    outputBox.insert(END,"Wybrales plik bin do uploadu: "+selectedZipFile+"\n\n", 'info')
    goToEnd();
    fileWindow.destroy()
    checkHowManyScriptsFiles();




def delteAllAfterStart():
    sourcePath = ''
    destPath = ''
    serviceName = ''
    itemsforlistbox = ''
    choosedLine = ''
    zipMavenFiles = []
    zipMavenFiles.clear()
    zipScriptsFile = []
    zipScriptsFile.clear()
    howManynotScriptsZipFilesCount = 0
    howManyScriptsZipFilesCount = 0
    #outputBox.delete('1.0', END)
    #sys.modules[__name__].__dict__.clear()
    window.mainloop()







def checkScriptsZipExists():
    global pathToSciptsFile
    global pathToBinZipFile
    pathToBinZipFile = (versionHttpFolder+"/"+binFile)
    pathToSciptsFile=(versionHttpFolder+"/"+scriptsZipFile)
    o = requests.head(pathToSciptsFile)
    if o:
        goToEnd();
        downloadBinFileToLocalPC();
    else:
        outputBox.insert(END, "Sprawdz nazwe pliku scripts w: "+sourcePath+"\n\n", 'warning')
        goToEnd();
        window.mainloop()


def downloadBinFileToLocalPC():
    global binLocal
    global pathToLocalAppVersion
    global scriptsZipFileStr
    pathToLocalAppVersion=(pathToCCHFiles+"\\"+serviceName+"\\"+version)
    os.makedirs(pathToCCHFiles+"\\"+serviceName+"\\"+version+"\\bin",exist_ok=True)
    binLocal=(pathToCCHFiles+"\\"+serviceName+"\\"+version+"\\bin")
    outputBox.insert(END,"Trwa sciaganie pliku: "+binFile+" do lokalnego folderu...\n", 'info')
    goToEnd();
    httpFileUrl = urllib.request.urlopen(pathToBinZipFile)
    httpFileSize = int((httpFileUrl.info()).get("Content-Length"))
    scriptsZipFileStr = str(scriptsZipFile)
    t = requests.get(pathToBinZipFile, stream=True)
    loadingBar = tkk.Frame(window, width=100, height=3)
    loadingBar.place(x=130, y=610)
    progressBar = ttk.Progressbar(loadingBar, orient='horizontal', length=705, mode='determinate')
    progressBar.pack(expand=True, fill=tkinter.BOTH, side=tkinter.TOP)
    with open(binLocal+"\\"+binFile, 'wb') as openBin:
        localBinFileSize = 0
        for chunk in t.iter_content(chunk_size=65536):
            openBin.write(chunk)
            openBin.flush()
            localBinFileSize = int((os.stat(binLocal+"\\"+binFile)).st_size)
            percent = ((localBinFileSize / httpFileSize)*100)
            if "." in str(percent):
                percent,rest = str(percent).split(".")
            progressBar["value"]=percent
            progressBar["maximum"]=100
            progressBar.update()
            loadingPercet = Label(window, text=percent+"%", font='Helvetica 10')
            loadingPercet.place(x=840, y=609)
            loadingPercet.update()
            loadingPercet.destroy()
    loadingBar.destroy()
    downloadScriptsFileToLocalPC();


def downloadScriptsFileToLocalPC():
    global scriptsLocal
    os.makedirs(pathToCCHFiles+"\\"+serviceName+"\\"+version+"\\scripts",exist_ok=True)
    scriptsLocal=(pathToCCHFiles+"\\"+serviceName+"\\"+version+"\\scripts")
    outputBox.insert(END,"Trwa sciaganie pliku: "+scriptsZipFileStr+" do lokalnego folderu...\n", 'info')
    goToEnd();
    r = requests.get(pathToSciptsFile, stream=True)
    localScriptsFileSize = 0
    loadingBar = tkk.Frame(window, width=100, height=3)
    loadingBar.place(x=130, y=610)
    progressBar = ttk.Progressbar(loadingBar, orient='horizontal', length=705, mode='determinate')
    progressBar.pack(expand=True, fill=tkinter.BOTH, side=tkinter.TOP)
    httpFileUrl = urllib.request.urlopen(pathToSciptsFile)
    httpFileSize = int((httpFileUrl.info()).get("Content-Length"))
    with open(scriptsLocal+"\\"+scriptsZipFileStr, 'wb') as openScripts:
        #openScripts.write(r.content)
        for chunk in r.iter_content(chunk_size=65536):
            openScripts.write(chunk)
            openScripts.flush()
            localScriptsFileSize = int((os.stat(scriptsLocal+"\\"+scriptsZipFileStr)).st_size)
            percent = ((localScriptsFileSize / httpFileSize)*100)
            if "." in str(percent):
                percent,rest = str(percent).split(".")
            progressBar["value"]=percent
            progressBar["maximum"]=100
            progressBar.update()
            loadingPercet = Label(window, text=percent+"%", font='Helvetica 10')
            loadingPercet.place(x=840, y=609)
            loadingPercet.update()
            loadingPercet.destroy()
        loadingBar.destroy()
    outputBox.insert(END,"Zapisano pliki w: "+pathToLocalAppVersion+"\n\n", 'info')
    goToEnd();
    unzipScripts();


def unzipScripts():
    outputBox.insert(END,"Wypakowuje skrypty...\n", 'info')
    goToEnd();
    ZipFile=''
    zip_ref = zipfile.ZipFile((scriptsLocal+"\\"+scriptsZipFileStr), 'r')
    zip_ref.extractall(pathToCCHFiles+"\\"+serviceName+"\\"+version)
    zip_ref.close()
    outputBox.insert(END,"Skrypty wypakowane!\n\n", 'info')
    goToEnd();
    os.remove(scriptsLocal+"\\"+scriptsZipFileStr)
    checkFTP();


def checkFTP():
    ftpp = ftplib.FTP(ftpServer,ftpUser,ftpPass)
    folderName = str(destPath+"/"+version)
    binFileExist = [x for x in (ftpp.nlst(destPath)) if folderName in x]
    oldVersions = int(str(binFileExist).count(version+'_OLD'))+1
    if folderName in binFileExist:
        overwriteFtp = messagebox.askyesno("Pytanko",("Taka wersja jest już na FTP!\n\nCzy mam obecna wersje wy-OLD-owac?\n\n"+"Nowa nazwa obecnej wersji na FTP to: "+folderName+'_OLD'+str(oldVersions)+"\n\n"+"Pobrana wersja bedzie zapisana pod: "+folderName+"\n\n"))
        overwriteFtp
        if overwriteFtp:
            outputBox.insert(END,'Obecna wersja zmienia nazwe na: '+folderName+'_OLD'+str(oldVersions)+"\n\n", 'info')
            goToEnd();
            ftpp.rename(folderName,folderName+'_OLD'+str(oldVersions))
            ftpp.close()
            updateBinOnFTP();
        else:
            outputBox.insert(END,"To wracamy na poczatek. Usuwam TempCH.\n", 'warning')
            goToEnd();
            shutil.rmtree(pathToCCHFiles, ignore_errors=True)
            window.mainloop();
    else:
        goToEnd();
        ftpp.close()
        updateBinOnFTP();

sizeWritten = 0


def binProgressBarDef(block):
    global sizeWritten
    global loadingPercet
    sizeWritten += 65536
    percentComplete = (str((sizeWritten / binFileSize)*100)[:4])
    #percent,rest = beforeZip,afterZip = percentComplete.split(".")
    if "." in percentComplete:
        percent,rest = percentComplete.split(".")
    else:
        percent = percentComplete
    loadingPercet = Label(window, text=percent+"%", font='Helvetica 10')
    loadingPercet.place(x=840, y=609)
    loadingPercet.update()
    progressBar["value"]=sizeWritten
    progressBar["maximum"]=binFileSize
    progressBar.update()
    loadingPercet.destroy()


def updateBinOnFTP():
    global progressBar
    global loadingBar
    global sizeWritten
    global percent
    global binFileSize
    binFileSize = (os.stat(binLocal+"\\"+binFile)).st_size
    outputBox.insert(END,'Uploaduje na FTP plik: '+binFile+"\n", 'info')
    goToEnd();
    session = ftplib.FTP(ftpServer,ftpUser,ftpPass)
    ftpBinFile = open(binLocal+'\\'+binFile,'rb')    #binFile to send

    # :::: PROGRESSBAR STYLE :::
    #s = ttk.Style()
    #s.theme_use('clam')
    #s.configure("red.Horizontal.TProgressbar", foreground='#ffa31a', background='#ffa31a')

    # Progressbar frame
    loadingBar = tkk.Frame(window, width=100, height=3)
    loadingBar.place(x=130, y=610)
    #If you want to change style add: style="red.Horizontal.TProgressbar", into 'progressBar = ttk.Progressbar'
    progressBar = ttk.Progressbar(loadingBar, orient='horizontal', length=705, mode='determinate')
    progressBar.pack(expand=True, fill=tkinter.BOTH, side=tkinter.TOP)
    session.mkd(destPath+"/"+version)
    session.mkd(destPath+"/"+version+'/bin')         #create direcotry with version\bin
    session.mkd(destPath+"/"+version+'/scripts')     #create direcotry with version\scripts
    session.storbinary('STOR '+destPath+'/'+version+'/bin/'+binFile,ftpBinFile,callback=binProgressBarDef,blocksize=65536)
    sizeWritten = 0
    percent = 0
    loadingBar.destroy()
    del progressBar
    del loadingBar
    loadingPercet.destroy()
    ftpBinFile.close()    # close file and FTP
    session.quit()
    goToEnd();
    listOfScriptsFiles();



def scriptsProgressBarDef(block):
    global sizeWritten
    global loadingPercet
    sizeWritten += 65536
    percentComplete = (str((sizeWritten / binFileSize)*100)[:4])
    if "." in percentComplete:
        percent,rest = percentComplete.split(".")
    else:
        percent = percentComplete
    loadingPercet = Label(window, text=percent+"%", font='Helvetica 10')
    loadingPercet.place(x=840, y=609)
    loadingPercet.update()
    progressBar["value"]=sizeWritten
    progressBar["maximum"]=scriptFileSize
    progressBar.update()
    loadingPercet.destroy()


def listOfScriptsFiles():
    global scriptFileSize
    global progressBar
    global loadingBar
    global sizeWritten
    files = []
    for r, d, f in os.walk(scriptsLocal):
        for file in f:
            if '.' in file:
                files.append(os.path.join(r, file))
    #outputBox.insert(END,'Wylistowane skyrpty do uploadu to: '+files+"\n", 'info')
    #goToEnd();
    for f in files:
        scriptFileToUpload = f.rsplit('\\',1)[1]
        outputBox.insert(END,'Uploaduje na FTP plik: '+scriptFileToUpload+"\n", 'info')
        goToEnd();
        session = ftplib.FTP(ftpServer,ftpUser,ftpPass)
        ftpScriptFile = open(f,'rb')                            #scriptFile to send
        scriptFileSize = (os.stat(scriptsLocal+"\\"+scriptFileToUpload)).st_size
        sizeWritten = 0
        # Progressbar frame
        loadingBar = tkk.Frame(window, width=100, height=3)
        loadingBar.place(x=130, y=610)
        #If you want to change style add: style="red.Horizontal.TProgressbar", into 'progressBar = ttk.Progressbar'
        progressBar = ttk.Progressbar(loadingBar, orient='horizontal', length=705, mode='determinate')
        progressBar.pack(expand=True, fill=tkinter.BOTH, side=tkinter.TOP)
        session.storbinary('STOR '+destPath+'\\'+version+'\\scripts\\'+scriptFileToUpload,ftpScriptFile,callback=scriptsProgressBarDef,blocksize=65536)
        sizeWritten = 0
        percent = 0
        loadingBar.destroy()
        loadingPercet.destroy()
        del progressBar
        del loadingBar
        ftpScriptFile.close()                              # close file and FTP
        session.quit()
    #for batFile in files:
    #    if ".bat" in batFile:
    #        batFileToUpload = batFile.rsplit('\\',1)[1]
    #        session = ftplib.FTP(ftpServer,ftpUser,ftpPass)
    #        ftpUploadFile = open(batFile,'rb')
    #        session.storbinary('STOR '+destPath+'\\'+version+'\\bin\\'+batFileToUpload,ftpUploadFile,blocksize=65536)
    #        ftpUploadFile.close()
    #        session.quit()
    #    elif ".exe" in batFile:
    #        batFileToUpload = batFile.rsplit('\\',1)[1]
    #        session = ftplib.FTP(ftpServer,ftpUser,ftpPass)
    #        ftpUploadFile = open(batFile,'rb')
    #        session.storbinary('STOR '+destPath+'\\'+version+'\\bin\\'+batFileToUpload,ftpUploadFile,blocksize=65536)
    #        ftpUploadFile.close()
    #        session.quit()
    outputBox.insert(END,"\n")
    goToEnd();
    finish();


def finish():
    outputBox.insert(END,'Gotowe. FTP pomyslnie zupdatowany. Usuwam lokalny folder tymczasowy\n', 'done')
    shutil.rmtree(pathToCCHFiles, ignore_errors=True)
    goToEnd();
    delteAllAfterStart();



def clearTextBox():
    outputBox.delete('1.0', END)
    window.mainloop()
    sys.modules[__name__].__dict__.clear()





# Window with information about FTP connection
def connectionInfo():
    connectionInfoWindow = Tk()
    connectionInfoWindow.title("Info o polaczeniu")
    connectionInfoWindowWidth = str(100+(7*(int(len("File with source and destination paths: "+srcDownloadFile)))))
    connectionInfoWindow.geometry(connectionInfoWindowWidth[:3]+'x200')
    ftpServerView = Label(connectionInfoWindow, text=('Serwer FTP: '+ftpServer), font='Helvetica 11')
    ftpServerView.grid(column=0, row=0, in_=connectionInfoWindow, sticky=W)

    ftpUserView = Label(connectionInfoWindow, text=('Server User: '+ftpUser), font='Helvetica 11')
    ftpUserView.grid(column=0, row=1, in_=connectionInfoWindow, sticky=W)

    pathToCCHFilesView = Label(connectionInfoWindow, text=("Temp folder path: "+pathToCCHFiles), font='Helvetica 11')
    pathToCCHFilesView.grid(column=0, row=2, in_=connectionInfoWindow, sticky=W)

    srcDownloadFileView = Label(connectionInfoWindow, text=("File with source and destination paths: "+srcDownloadFile), font='Helvetica 11')
    srcDownloadFileView.grid(column=0, row=3, in_=connectionInfoWindow, sticky=W)

    # FTP Connection check
    isconnectedLabel = Label(connectionInfoWindow, text=("FTP connection problems!"), font='Helvetica 11', bg="red")
    isconnectedLabel.grid(column=0, row=4, in_=connectionInfoWindow, sticky=W)
    isFtpAvaliable = ftplib.FTP(ftpServer, ftpUser, ftpPass)
    if isFtpAvaliable:
        isconnectedLabel = Label(connectionInfoWindow, text=("   FTP connection works!   "), font='Helvetica 11', bg="green")
        isconnectedLabel.grid(column=0, row=4, in_=connectionInfoWindow, sticky=W)
    else:
        isconnectedLabel = Label(connectionInfoWindow, text=("FTP connection problems!"), font='Helvetica 11', bg="red")
        isconnectedLabel.grid(column=0, row=4, in_=connectionInfoWindow, sticky=W)



#Buttons
searchButton = Button(window, text="Search",command=startup)
searchButton.place(x=535, y=135, in_=window,width=100,height=70)
clearButton = Button(window, text="Clear\ncache and \nlog window",command=clearTextBox)
clearButton.place(x=650, y=135, in_=window,width=100,height=70)
connectionInfoButton = Button(window, text="Connection\ninfo",command=connectionInfo)
connectionInfoButton.place(x=765, y=135, in_=window,width=100,height=70)





window.mainloop()